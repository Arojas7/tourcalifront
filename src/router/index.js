import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import sitios from '../components/sitios.vue'
import Guia from '../components/Guia.vue'
import Mapa from '../components/Mapa.vue'
import Htour from '../components/Htour.vue'
import Conectar from '../components/Conectar.vue'
import Galeria from '../components/Galeria.vue'
import Actualizaruser from '../components/Actualizaruser.vue'
import Login from '../views/Login.vue'
import Registrar from '../views/Registrar.vue'
import Recuperarclave from '../views/Recuperarclave.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/Home',
    name: 'Home',
    component: Home
  },

  {
    path: '/',
    name: 'login',
    component: Login
  },

  {
    path: '/Registrar',
    name: 'Registrar',
    component: Registrar
  },

  {
    path: '/Recuperarclave',
    name: 'Recuperarclave',
    component: Recuperarclave
  },

  {
    path: '/sitios',
    name: 'sitios',
    component: sitios
  },

  {
    path: '/Guia',
    name: 'Guia',
    component: Guia
  },

  {
    path: '/Mapa',
    name: 'Mapa',
    component: Mapa
  },

  {
    path: '/Htour',
    name: 'Htour',
    component: Htour
  },

  {
    path: '/Conectar',
    name: 'Conectar',
    component: Conectar
  },

  {
    path: '/Galeria',
    name: 'Galeria',
    component: Galeria
  },

  {
    path: '/Actualizaruser',
    name: 'Actualizaruser',
    component: Actualizaruser
  }

 
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
